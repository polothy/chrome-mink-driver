Changelog
=========

## 2.1.1

* Fixed compatibility with 5.6 and 7.0

## 2.1.0

* Added support for switching to popups which chrome turned opened as tabs

* Improved findElementXpaths to get the shortest xpath possible

* Fixed xpath queries not always returning the elements in the correct order

* Fixed setValue not always triggering keyup/keydown

* Fixed popup blocker stopping popups triggered by click

* Fixed deadlock when javascript prompt/alert is shown

* Fixed double click not dispatching an event for the first click

* Fixed double click not bubbling

* Fixed page load timing out after 5 seconds

## 2.0.1

* Removed behat dependency

## 2.0.0

* Fixed screenshot feature (thanks https://gitlab.com/OmarMoper)

* Extracted behat extension to its own repository

## 1.1.3

* Fixed timeout when checking for the status code of a request served from cache

## 1.1.2

* PHP 5.6 support

* Fixed websocket timeout when visit() was not the first action after start() or reset()

## 1.1.1

* Licensed as MIT

## 1.1.0

* Added support for basic http authentication

* Added support for removing http-only cookies

* Added support for file upload

* Fixed getContent() returning htmldecoded text instead of the outer html as is

## 1.0.1

* Fixed back() and forward() timing out when the page is served from cache.
